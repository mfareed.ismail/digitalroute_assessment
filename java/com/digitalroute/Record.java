package com.digitalroute;

public class Record {

	private String callId;
	private String aNum;
	private String bNum;
	private int seqNum;
	private byte causeForOutput;
	private int duration;

	public void setCallId(String callId) {
		this.callId = callId;
	}

	public void setSeqNum(int seqNum) {
		this.seqNum = seqNum;
	}

	public void setaNum(String aNum) {
		this.aNum = aNum;
	}

	public void setbNum(String bNum) {
		this.bNum = bNum;
	}

	public void setcauseForOutput(byte causeForOutput) {
		this.causeForOutput = causeForOutput;
	}

	public void setduration(int duration) {
		this.duration = duration;
	}

	public String getCallId() {
		return callId;
	}

	public int getSeqNum() {
		return seqNum;
	}

	public String getaNum() {
		return aNum;
	}

	public String getbNum() {
		return bNum;
	}

	public byte getcauseForOutput() {
		return causeForOutput;
	}

	public int getduration() {
		return duration;
	}
}
