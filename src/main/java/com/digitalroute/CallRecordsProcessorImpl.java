package com.digitalroute;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Scanner;

import com.digitalroute.input.CallRecordsProcessor;
import com.digitalroute.output.BillingGateway;

public class CallRecordsProcessorImpl implements CallRecordsProcessor, BillingGateway {

	String callId;
	int seqNum;
	String aNum;
	String bNum;
	byte causeForOutput;
	int duration;
	long totalDuration;
	ErrorCause errorCause;

	public CallRecordsProcessorImpl(BillingGateway billingGateway) {
		System.out.println("CallRecordsProcessorImpl constructor");
		billingGateway.beginBatch();
		billingGateway.consume(callId, seqNum, aNum, bNum, causeForOutput, duration);
		billingGateway.endBatch(totalDuration);
		billingGateway.logError(errorCause, callId, seqNum, aNum, bNum);
	}

	@Override
	public void processBatch(InputStream in) {

		System.out.println("CallRecordsProcessorImpl processBatch");

		beginBatch();

		@SuppressWarnings("resource")
		Scanner sc = new Scanner(in);

		// Reader inputStreamReader = new InputStreamReader(in);
		// BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		// String str;
		HashMap<String, Record> cdrMap = new HashMap<String, Record>();

		try {
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				String[] csvColumns = line.split("[:,\n]");
				try {
					if (csvColumns.length > 0) {
						Record record;
						String objectKey = csvColumns[2] + csvColumns[3];
						if (cdrMap.containsKey(objectKey)) {
							// System.out.println("Elemen exists, perform aggregation");
							record = (Record) cdrMap.get(objectKey);

							if (record.getSeqNum() == Integer.parseInt(csvColumns[1])) {
								// System.out.println("Elemen exists, sequence number is same, skipped record");
								ErrorCause errorCause = ErrorCause.DUPLICATE_SEQ_NO;

								consume(record.getCallId(), record.getSeqNum(), record.getaNum(), record.getbNum(),
										record.getcauseForOutput(), record.getduration());

								logError(errorCause, record.getCallId(), record.getSeqNum(), record.getaNum(),
										record.getbNum());

								totalDuration = +record.getduration();
								cdrMap.remove(objectKey);

								continue;
							}
							// System.out.println("cause of output = " + Integer.parseInt(csvColumns[4]) );

							if (Integer.parseInt(csvColumns[4]) == 1) {
								// System.out.println("Elemen exists, call is ongoing");
								record.setduration(Integer.parseInt(csvColumns[5]) + record.getduration());
								record.setSeqNum(Integer.parseInt(csvColumns[1]));

							} else if (Integer.parseInt(csvColumns[4]) == 2) {
								// System.out.println("Elemen exists, call is end of call, trigger consume");
								record.setduration(Integer.parseInt(csvColumns[5]) + record.getduration());

								consume(record.getCallId(), record.getSeqNum(), record.getaNum(), record.getbNum(),
										record.getcauseForOutput(), record.getduration());

								totalDuration = +record.getduration();
								cdrMap.remove(objectKey);
							} else {
								// System.out.println("Elemen exists, call is incomplete log an error");
								record.setduration(Integer.parseInt(csvColumns[5]) + record.getduration());

								consume(record.getCallId(), record.getSeqNum(), record.getaNum(), record.getbNum(),
										record.getcauseForOutput(), record.getduration());

								totalDuration = +record.getduration();
								cdrMap.remove(objectKey);
							}

						} else {
							// System.out.println("Elemen not exists, add new element");
							record = new Record();
							record.setCallId(csvColumns[0]);
							record.setSeqNum(Integer.parseInt(csvColumns[1]));
							record.setaNum(csvColumns[2]);
							record.setbNum(csvColumns[3]);
							record.setcauseForOutput(Byte.parseByte(csvColumns[4]));
							record.setduration(Integer.parseInt(csvColumns[5]));
							cdrMap.put(objectKey, record);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (sc.hasNextLine() == false) {
					System.out.println("Reach end of file");
					cdrMap.forEach((k, v) -> {
						if (v.getcauseForOutput() != 0) {
							consume(v.getCallId(), v.getSeqNum(), v.getaNum(), v.getbNum(), v.getcauseForOutput(),
									v.getduration());

							totalDuration = +v.getduration();
						} else {
							ErrorCause errorCause = ErrorCause.NO_MATCH;
							logError(errorCause, v.getCallId(), v.getSeqNum(), v.getaNum(), v.getbNum());
						}
					});
				}

			}

			in.close();

			endBatch(totalDuration);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void beginBatch() {

		System.out.println("CallRecordsProcessorImpl start reading a file");
	}

	@Override
	public void consume(String callId, int seqNum, String aNum, String bNum, byte causeForOutput, int duration) {

		System.out.println("consume: " + callId + ", " + seqNum + ", " + aNum + ", " + bNum + ", " + causeForOutput
				+ ", " + duration);
	}

	@Override
	public void endBatch(long totalDuration) {

		System.out.println("endBatch: totalDuration " + totalDuration);
	}

	@Override
	public void logError(ErrorCause errorCause, String callId, int seqNum, String aNum, String bNum) {

		System.out.println("logError " + errorCause + ", " + callId + ", " + seqNum + ", " + aNum + ", " + bNum);
	}

}
